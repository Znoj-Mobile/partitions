### **Description**
App listing partitions with used and free space in android devices

---
### **Technology**
Android

---
### **Year**
2015

---
### **Screenshot**

![](/README/Screenshot_2018-08-12-18-01-15-157_com.example.iri.partitions.png)
