package com.example.iri.partitions;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class partitions extends ActionBarActivity {

    ArrayList<Map<String, String>> list;
    ListView listView;
    SimpleAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partitions);

        Button listing = (Button) findViewById(R.id.button);

        final OutputStream o = null;
        final Process[] process = {null};
        byte[] buffer = new byte[1024];
        listing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //todo
                    process[0] = new ProcessBuilder()
                            .command("df")
                            .redirectErrorStream(true)
                            .start();
                InputStream in2 = process[0].getInputStream();
                    process[0].waitFor();
                readStream(in2, o);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        String[] from = {"fileSystem", "size", "used" ,"free"};
        int[] to = {R.id.fileSystem, R.id.size, R.id.used, R.id.free };

        list = new ArrayList<Map<String,String>>();
        listView = (ListView) findViewById(R.id.listView);

        adapter = new SimpleAdapter(this, list, R.layout.list_item, from, to);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_partitions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void readStream(InputStream in2, OutputStream out2) {
        BufferedReader in = new BufferedReader(new InputStreamReader(in2));
        //BufferedWriter out = new BufferedWriter(new OutputStreamWriter(out2));

        try {
            String tmp = in.readLine();
            list.clear();
            while(tmp != null) {
                Log.d("readStream", tmp);
                if(!tmp.contains("denied")){
                    list.add(put_data(tmp));
                }
                //out.write(tmp);
                //out.write("<br />");
                tmp = in.readLine();
            }
            //out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            adapter.notifyDataSetChanged();
        }

    }

    private Map<String, String> put_data(String tmp) {
        HashMap<String, String> item = new HashMap<String, String>();

        //split
        String[] separated = tmp.split("\\s+");
        if(separated.length == 3){
            item.put("fileSystem", tmp);
            return item;
        }
        item.put("fileSystem", separated[0]);
        item.put("size", separated[1]);
        item.put("used", separated[2]);
        item.put("free", separated[3]);
        return item;
    }
}
